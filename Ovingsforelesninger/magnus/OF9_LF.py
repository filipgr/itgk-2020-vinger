# Oppgave 1
arr = [32, 5, 2, 1,44 ,5 ,6, 2222, 55, 33]

def seq(liste, tall):
    for e in liste:
        if e == tall:
            return True
    return False

# Oppgave 2
def bin_search(liste, tall, min, max):
    if(min > max):
        return -1

    mid = (max+min) // 2
    if liste[mid] == tall:
        return mid
    if (tall < liste[mid]):
        return bin_search(liste, tall, min, mid - 1)
    else:
        return bin_search(liste, tall, mid + 1, max)

arr = [3, 5, 20, 44, 56, 75, 77, 100]
print(bin_search(arr, 75, 0, len(arr)-1))


# Oppgave fra Insperaøving 2
def get_prioritized_list(olist):
    nlist = list(olist)
    for i in range(1, len(nlist)):
        curVal = nlist[i]
        curPos = i
        while curPos > 0 and nlist[curPos - 1][1] < curVal[1]:
            nlist[curPos] = nlist[curPos - 1]
            curPos -= 1
        nlist[curPos] = curVal
        while curPos > 0 and nlist[curPos - 1][1] == curVal[1] and nlist[curPos - 1][0] > curVal[0]:
            nlist[curPos] = nlist[curPos - 1]
            curPos -= 1
        nlist[curPos] = curVal
    return nlist

print(get_prioritized_list([("Dogs", 8), ("Cats", 9), ("Chocolate", 10), ("Pancakes", 7), ("Ice cream", 4)]))
print(get_prioritized_list([ ("Dogs", 8), ("Cats", 8),  ("Chocolate", 8), ("Pancakes", 8), ("Ice cream", 8)]))
print(get_prioritized_list([ ("Dogs", 10),("Cats", 10), ("Chocolate", 8), ("Pancakes", 10), ("Ice cream", 9)]))
print(get_prioritized_list([ ("abc", 3), ("abc", 9), ("bcd", 56), ("def", 3), ("bcd", 2), ("lkj", 3), ("thy", 9)]))


# Oppgave 3
def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n-1)

print(factorial(5))


# Oppgave 4
def binomial(n, k):
    return factorial(n) / (factorial(k) * factorial(n - k))

print(binomial(10, 4))


# Oppgave 5
def sum_list(liste):
    sum = 0
    for elem in liste:
        if isinstance(elem, list):
            sum += sum_list(elem)
        else:
            sum += elem
    return sum

print(sum_list([[4,5,1], 5, 2, 8, [2], [21, [9123,[23] ,3], 2]]))
