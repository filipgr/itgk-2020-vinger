# Del 1
def do_user_like(items):
    scores = []
    print("On a scale from 1 to 10 where 10 is the highest, how much do you like: ")
    for item in items:
        val = int(input(item + "? "))
        while not 0 < val <= 10:
            print("You have to give a value in the interval [1, 10]. Try again.")
            val = int(input(item + "? "))
        score = (item, val)
        scores.append(score)
    return scores


# Del 2
def copy_list(lst):
    copy = []
    for i in range(len(lst)):
        copy.append(lst[i])
    return copy


def get_prioritized_list(scores):
    pri_list = copy_list(scores)
    i = 1
    while i < len(pri_list):
        j = i
        while j > 0 and (pri_list[j-1][1] < pri_list[j][1] or (pri_list[j-1][1] == pri_list[j][1] and pri_list[j-1][0] > pri_list[j][0])):
            hold_score = pri_list[j]
            pri_list[j] = pri_list[j-1]
            pri_list[j-1] = hold_score
            j -= 1
        i += 1
    return pri_list


# Del 3
def what_user_likes_best(items, num):
    if num < 1 or num > len(items):
        print("Invalid number given.")
        return
    scores = do_user_like(items)
    pri_lst = get_prioritized_list(scores)
    if num == 1:
        print("Your number", num, "is: ")
    else:
        print("Your top", num, "are: ")
    for i in range(num):
        print(str(i+1)+".", pri_lst[i][0])


# what_user_likes_best(['Dogs', 'Cats', 'Chocolate', 'Pancakes', 'Ice cream'], 5)
