# Del 1
def ask_question(str):
        return input(str)

# Del 2
def spam_with_questions(str):
        answer = ""
        while answer.lower() != 'stopp':
                answer = ask_question(str)

# Del 3
def energy_efficient_spamming(str1, str2):
        if len(str2) > len(str1):
                return
        answer = input(str1)
        if answer.lower() != "stopp":
                spam_with_questions(str2)
