l, b, h = 0, 0, 0

def getInputs():
    global l, b, h
    l = float(input("Hva er lengden? "))
    b = float(input("Hva er bredden? "))
    h = float(input("Hva er høyden? "))

getInputs()

while (l == b or l == h or h == b):
    print("Morten er ikke fornøyd. Prøv en gang til.")
    getInputs()

print("Den største kuben vil ha et volum på", round(min(l, b, h)**3, 2))
