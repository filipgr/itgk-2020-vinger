def chord_notes(chord):
    chords_intervals = {
        '':(0, 4, 3),
        'm':(0, 3, 4),
        'dim':(0, 3, 3),
        'aug':(0, 4, 4),

        '6':(0, 4, 3, 2),
        '7':(0, 4, 3, 3),
        'maj7':(0, 4, 3, 4),

        'm6':(0, 3, 4, 3),
        'm7':(0, 3, 4, 4),

        'dim7':(0, 3, 3, 3),

        'sus4':(0, 5, 2),
        'm7b5':(0, 3, 3, 4)
    }

    undertones = get_undertone(chord)
    root = undertones[0]
    chord_type = undertones[1]

    tone_list = get_tone_list(chord)

    tone_index = tone_list.index(root)
    chord_notes = []
    for interval in chords_intervals[chord_type]:
        tone_index += interval
        tone_index = tone_index % 12
        chord_notes.append(tone_list[tone_index])

    return chord_notes


def get_tone_list(chord):
    cross_tones = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
    b_tones = ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B']

    undertone = get_undertone(chord)
    root = undertone[0]
    chord_type = undertone[1]

    # Her definerer vi C som kryss-toneart og tar ikke hensyn til enharmoniske feil
    # Gjør også et par andre forenklinger som ikke er helt musikkteoretisk riktige
    sharp_major_keys = ['C', 'G','D', 'A', 'E', 'B', 'F#', 'Cb', 'Gb', 'Db']
    sharp_minor_keys = ['A', 'E', 'B', 'F#', 'C#', 'G#', 'D#', 'A#',  'Ab', 'Eb', 'Bb']

    #flat_major_keys = ['F', 'Bb', 'Eb', 'Ab', 'Db', 'Gb', 'Cb', 'C#', 'F#', 'B']
    #flat_minor_keys = ['A', 'D', 'G', 'C', 'F', 'Bb', 'Eb', 'Ab', 'A#', 'D#', 'G#']

    if not chord_type.startswith('m') and root in sharp_major_keys:
        return cross_tones

    if chord_type.startswith('m') and root in sharp_minor_keys:
        return cross_tones

    else:
        return b_tones

def get_undertone(chord):
    tone = chord[0].upper() # Tar ut første bokstav
    if len(chord) > 1 and chord[1] in ['b', '#']:
        tone += chord[1].lower()
        chordtype = chord[2:]
    else:
        chordtype = chord[1:]

    return (tone, chordtype)

# Lagt inn i forrige oppgave

print(chord_notes("Gmaj7"))
print(chord_notes("C#m7b5"))
